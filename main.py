import sys
import json
import glob

import jsonlines
from loguru import logger

import settings


def _load_json_file(file) -> list:
    with open(file, encoding="utf8") as f:
        ads = json.load(f)
    logger.info(f"Loaded {len(ads)} ads from {file}")
    return ads


def select_ads(ads) -> list:
    """
    :param ads:
    :return:
    """
    active_ads = []
    year = int(settings.year)
    for ad in ads:
        if ad["removed"]:
            # Since the indata is from JobStream, there will be ads that are marked as removed
            continue
        elif ad["publication_date"].startswith(f"{year + 1}"):
            #  file name pattern might include ads from next year
            continue
        else:
            active_ads.append(ad)
    return active_ads


def get_file_list() -> list:
    """
    :return: file list, sorted with latest first so that the first time an ad is read, it's the latest version
    """
    pattern = settings.stream_ads_folder + f"*from_{settings.year}*"
    files = glob.glob(pattern)
    logger.info(f"Found {len(files)} files matching pattern: '{pattern}'")
    if (len(files)) == 0:
        logger.info("Exiting, since no files was found...")
        sys.exit()
    return sorted(files, reverse=True)


def main() -> int:
    logger.info("start")
    files = get_file_list()
    duplicates = 0
    errors = 0
    processed_ids = set()
    total_ads = 0
    outfile_name = f"{settings.year}_summarized.jsonl"

    with jsonlines.open(outfile_name, "w") as outfile:
        for file in files:
            ads_from_file = _load_json_file(file)
            ads = select_ads(ads_from_file)
            for ad in ads:
                total_ads += 1
                ad_id = ad["id"]
                if ad_id not in processed_ids:
                    outfile.write(ad)
                    processed_ids.add(ad_id)
                else:
                    logger.trace(f"{ad_id} already processed")
                    duplicates += 1
    logger.debug(f"Total including duplicates: {total_ads} in {len(files)} files. Duplicates: {duplicates}")
    logger.info(f"Finished, wrote {len(processed_ids)} ads to {outfile_name}")

    return 0

if __name__ == "__main__":
    sys.exit(main())
