import jsonlines
from loguru import logger
from settings import OUTPUT_FILE, LATEST_FILE_PARTIAL_YEAR, PREVIOUS_FILE_PARTIAL_YEAR

"""
load latest file
write to combined file and save ids
load previous file
if ad is already saved it´s discarded, else it´s saved to the combined file
"""

saved_ads = 0
with jsonlines.open(OUTPUT_FILE, "w") as outfile:
    processed_ids = set()
    with jsonlines.open(LATEST_FILE_PARTIAL_YEAR) as latest_file:
        # Start with saving all ads from the latest file, the version here is the latest
        logger.info(f"Reading most recent file: {LATEST_FILE_PARTIAL_YEAR}")
        for ad in latest_file:
            processed_ids.add(ad["id"])
            outfile.write(ad)
            saved_ads += 1
        logger.info(f"Finished processing {LATEST_FILE_PARTIAL_YEAR}")
    with jsonlines.open(PREVIOUS_FILE_PARTIAL_YEAR) as previous_part_of_year_file:
        # Read through file created earlier, if an ad was found in the latest file in the previous step,
        # we ignore it here since it's an older version
        logger.info(f"Reading earlier part of the year: {PREVIOUS_FILE_PARTIAL_YEAR}")
        for ad in previous_part_of_year_file:
            ad_id = ad["id"]
            if ad_id in processed_ids:
                continue
            else:
                outfile.write(ad)
                processed_ids.add(ad["id"])
                saved_ads += 1
        logger.info(f"Finished processing: {PREVIOUS_FILE_PARTIAL_YEAR}")

logger.info(f"Finished, saved: {saved_ads} ads , processed: {len(processed_ids)} ads to {OUTPUT_FILE}")
