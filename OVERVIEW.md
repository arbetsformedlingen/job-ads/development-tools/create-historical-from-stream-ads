# Creation of historical ads

## This is a description of a legacy system that will be replaced by [historical-importer](https://gitlab.com/arbetsformedlingen/job-ads/historical-importer)

## Flowchart

```mermaid
flowchart TD
    A[JobStream] <-->|Get ads| B(Extract-af)
    B -->|Save ads| C[Minio]
    C --> D[manual work: save from Minio & summarize]
    D --> E[Anonymize]
    E --> F{Q1?}
    F -->|yes| H[manual work: upload to data.jobtechdev.se]
    F -->|No| G[manual work: summarize with previous quarters]
    G --> H[publish to data.jobtechdev.se]
    H --> I[manual work: conversion, enrichment,  import to opensearch and upload to data.jobtechdev.se ]
```

## Overview

1. Ads are fetched from JobStream. New, updated, removed.
2. The ads are saved as json files to a Minio bucket.
3. Quarterly, they are summarized into a single file.
4. That file is anonymized.
5. If it's not the first quarter, the resulting files is added to the file for the previous quarter(s). Duplicates
   between files are removed, the latest version of the ad is kept.
6. The resulting file is manually uploaded to data.jobtechdev.se
7. The file is run through the various steps in jobsearch-importers to prepared and ultimately imported to OpenSearch.

From 2021-01-01 all steps can be done. The Daily files from that date and onward are available on Minio.
For earlier years (2016-2020) step 7 is possible.
For even earlier years, they are only available as files. Adding code to handle missing data and other issues would be
too much work. The data quality is too low to be useful in a search-api.

## Logins required
- Openshift testing
- GitLab
- Minio (link at the end)
- AWS S3
- JobTech dockerhub


## Collection of raw data.

The program `extractor-af` get changed (new, updated, deleted) ads every night and save them as json files to a Minio
bucket. In that bucket, files from 2021-01-01 and later are available.  
Since `extract-af` takes everything, there are ads included in the daily dumps that are still published.  
If an ad is changed at a later date, it's added to a new daily file.
This leads to some problems when an ad is included in historical files for multiple years.


e.g.:  
An ad is added on 2022-11-29 and dumped to daily file.  
It's updated on 2022-12-04 and dumped to that daily file.  
At the beginning of January 2023 the summarization and de-duplication is done. The version from 2022-12-04 is kept since it's the latest.  
The next year, on 2023-01-12, the ad is updated again and saved to a daily file.  
At the beginning of April 2023 the daily files for 2023 Quarter 1 are collected and summarized. The ad will be included in this file as well.

### Code repository

The code is copied from a [private repo](https://gitlab.com/simonbe1/extractors-af) to an [archived repo under jobads](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/extractors-af)

### Storage on Minio

The json-files are saved into the folder "stream-ads"
in [Minio: "employer"](https://minio-console.arbetsformedlingen.se/buckets/employer/browse/c3RyZWFtX2Fkcy8=)
The reason that the files are saved into that bucket is probably because the person who did it had access and it was the easiest option.
The bucket name and path is hardcoded, a build of a new Docker image is needed to change it.

### Docker image

The docker image is only published on a [personal account on Dockerhub](simonbe333/extractor-af) It's not published on
JobTech Docker Mirror.

### Openshift

It runs in ["testing"-openshift](https://console-openshift-console.apps.testing.services.jtech.se/k8s/cluster/projects/extractor-af) as a cronjob

The configuration is saved
as ["environment"](https://console-openshift-console.apps.testing.services.jtech.se/k8s/ns/extractor-af/pods/cron-extractor-af-28303200-czhg8/environment)
to the cronjob.
Changes in the configuration can be done by manually editing
the [yaml-file](https://console-openshift-console.apps.testing.services.jtech.se/k8s/ns/extractor-af/cronjobs/cron-extractor-af/yaml)
in the OpenShift GUI.
There is no sync with external configuration as in other projects.

## How to summarize daily files into a quarterly file
The code for summarizing ads was written from scratch when responsibility was transferred. The previous way of doing it is unknown.

- log into Minio and download the files you want to summarize. No automation, select files and download.

### Quarter 1

- Summarize using this repo
- [anonymize](https://gitlab.com/arbetsformedlingen/libraries/anonymisering) the summarized file

### Quarter 2, 3, 4

2nd, 3rd and 4th quarter:  summarize ads as in Q1, anonymize and combine with previous quarter(s) using `combine_with_previous_quarters.py`.

Name the file in a descriptive way, check existing files to see how it has been done.

### Upload to data.jobtechdev.se

Upload the file to data.jobtechdev.se using the program [mc](https://min.io/docs/minio/linux/reference/minio-mc.html)
You need an AWS account for this and need to create an `accessKey` and a `secretKey` for your mc configuration.
Upload is from the command line using `mc`, upload through the web gui does not work.
Check with Team Calamari if you do not have an AWS account or lack the necessary permissions.

Create a configuration file in the same folder as `mc.exe`.

E.g. `config.json`
```json
{
	"version": "10",
	"aliases": {
		"s3": {
			"url": "https://s3.amazonaws.com",
			"accessKey": "YOUR-ACCESS-KEY-HERE",
			"secretKey": "YOUR-SECRET-KEY-HERE",
			"api": "S3v4",
			"path": "off"
		}
	}
}
```

in Powershell, copy the file to AWS S3:
```
 .\mc.exe --insecure --config-dir . cp .\<NEW-FILE>.zip s3/data.jobtechdev.se-adhoc/annonser/historiska/
```


### Processing and import to OpenSearch

These steps are done
in  [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers/-/blob/main/docs/historical_importer.md)

- Conversion
- Enrichment
- Import to OpenSearch
- Creation of file variants
- Upload to data.jobtechdev.se

There is a [pipeline](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers-infra/-/blob/main/examples/README_start_import_manually.md) that currently is in an unknown state after attempts to upgrade Tekton.
Until that is working the steps in jobsearch-importers needs to be done manually on your computer.

## Risks with current solution:

- The code is not maintained and there are a number of security issues in the Docker image that is used.
- Risk of incompatibility if Minio is upgraded.
- The Docker image is only stored in a personal Dockerhub account
- The Minio password might be changed by someone who doesn't realize that it's also used by extractor-af (this has
  happened)
- An ad could be published and unpublished before it's collected. It can happen if the ad is only published part of a day, or if there are some problem with getting new ads from Ledigt Arbete into JobStream.

## Code changes in extract-af:

This has never been done, but at least these steps would have to be done:

- Clone the repository
- Un-archive the repository
- Create an issue and merge request as usual.
- Do code changes, review the merge request and merge.
- Archive the code repo.
- Build a Docker image locally and push it to JobTech Dockerhub with a tag for version. There are no automatic builds
  for this project.
- Change configuration in the [cron-job](https://console-openshift-console.apps.testing.services.jtech.se/k8s/ns/extractor-af/cronjobs/cron-extractor-af/yaml)
  to use the new Docker image. Edit in OpenShift gui and save.

This is how it could look like in `cronjobs/cron-extractor-af/yaml` after a build and push of a new Docker image:

```yaml
containers:
  - name: extractor-af
image: path-to-jobtech-dockerhub/extractor-af:TAG
```

Again: This has never been done. Don't expect it to be easy.


# Earlier ways of producing historical ads.

Pre-2021 they were allegedly done using database dumps from AIS, but the procedure for that is unknown.


# Useful links

- Openshift namespace: https://console-openshift-console.apps.testing.services.jtech.se/k8s/cluster/projects/extractor-af
- GitLab repo for extractors-af: https://gitlab.com/arbetsformedlingen/job-ads/development-tools/extractors-af
- Minio bucket: https://minio-console.arbetsformedlingen.se/buckets/employer/browse/c3RyZWFtX2Fkcy8=
- Login to the Minio bucket above: https://vaultwarden-test.jobtechdev.se/#/vault?itemId=d3463bbb-834a-4f38-93be-ccc5830faa3c&cipherId=a01f9457-c1f7-4e05-9f67-73126e0901a4
- data.jobtechdev.se in AWS S3: https://s3.console.aws.amazon.com/s3/buckets/data.jobtechdev.se-adhoc?region=eu-north-1&prefix=annonser/historiska/&showversions=false
- Support agreement: https://serviceportal.arbetsformedlingen.se/now/nav/ui/classic/params/target/kb_knowledge.do%3Fsys_id%3D0b91880965d741103a06ae19d96e61d0
- data.jobtechdev.se https://data.jobtechdev.se/annonser/historiska/index.html
