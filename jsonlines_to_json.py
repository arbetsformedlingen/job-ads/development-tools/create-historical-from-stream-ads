import sys
import json
import jsonlines
from loguru import logger


def convert_jsonlines_to_json():
    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]
    logger.info(f"Input: {input_file_name}, output: {output_file_name}")

    with jsonlines.open(input_file_name) as infile, open(output_file_name, mode="w", encoding="utf8") as outfile:
        ads = []
        for ad in infile:
            ads.append(ad)
        logger.info("Read all ads, saving as json")
        json.dump(ads, outfile)
        logger.info("Complete")


if __name__ == '__main__':
    """
    Usage: python jsonlines_to_json.py <input-file.jsonl> <output-file.json>

    """

    convert_jsonlines_to_json()
