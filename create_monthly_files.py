import glob
import pathlib
import jsonlines
from loguru import logger

import settings


def save_to_monthly_file(ad: dict, output_dir: pathlib.Path):
    """
    Appends the ad to the file matching the ad's publication year and month
    """
    year = ad["publication_date"].split("-")[0]
    month = ad["publication_date"].split("-")[1]
    if year == settings.year:
        file_name = output_dir / f"{year}_{month}.jsonl"
        with jsonlines.open(file_name, "a") as outfile:
            outfile.write(ad)
    else:
        logger.trace(f"skipping {year}-{month}")


def _create_output_directory() -> pathlib.Path:
    output_dir = pathlib.Path(settings.MONTHLY_OUTPUT_DIR)
    output_dir.mkdir(exist_ok=True)
    logger.info(f"created output directory: {output_dir}")
    return output_dir


if __name__ == '__main__':
    # pattern that matches one or more anonymized files, reversed so that the latest file is processed first
    input_files = sorted(glob.glob(settings.MONTHLY_INPUT_DIR + settings.MONTHLY_PATTERN), reverse=True)

    logger.debug(f"Input files: {input_files}")
    output_dir = _create_output_directory()
    processed_ids = set()
    for file in input_files:
        logger.info(f"Processing {file}")
        counter = 0
        with jsonlines.open(file) as infile:
            for ad in infile:
                ad_id = ad["id"]
                if ad_id not in processed_ids:
                    save_to_monthly_file(ad, output_dir)
                    counter += 1
                    processed_ids.add(ad_id)
                else:
                    logger.trace(f"Already saved a later version of {ad_id}")

        logger.info(f"File:{file}, number of saved ads: {counter}")
    logger.info("completed")
