import os


stream_ads_folder = os.getenv("STREAM_ADS_FOLDER", "stream_ads")


year = os.getenv("YEAR", "2023")

MONTHLY_INPUT_DIR = os.getenv("INPUT_DIR_FOR_MONTHLY", "monthly_files_input")
MONTHLY_OUTPUT_DIR = os.getenv("MONTHLY_DIR_OUT", "monthly_files_output")
MONTHLY_PATTERN = os.getenv("MONTHLY_PATTERN", "/*.jsonl")

OUTPUT_FILE = os.getenv("OUTPUT_FILE_COMBINE_FILES")
LATEST_FILE_PARTIAL_YEAR = os.getenv("LATEST_FILE_PARTIAL_YEAR")
PREVIOUS_FILE_PARTIAL_YEAR = os.getenv("PREVIOUS_FILE_PARTIAL_YEAR")
