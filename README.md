# create-historical-from-stream-ads

## Overview
Start by reading `OVERVIEW.md`

## Description

This project summarizes json files that contains the output from the JobStream API saved as individual files with date
as part of the file name.
It starts with the latest file (file list from `glob` sorted in reverse order).
This order is important to save the latest version of the ad in the summary file.

## Installation

This project uses [Poetry](https://python-poetry.org/) for dependency management.  
Install Poetry with `pip install poetry`
Install dependencies with `poetry install`

## Usage
### Yearly file (or part of year)
Download daily files manually from Minio (bucket: employer / folder: stream_ads) to a local folder. 
Set environment variables for that folder (`STREAM_ADS_FOLDER`) and year (`YEAR`)
run `python main.py`
the output will be saved in `YYYY_summarized.jsonl` where YYYY is the environment variable `YEAR`

The output should be anonymized using https://gitlab.com/arbetsformedlingen/libraries/anonymisering and the anonymized file used 
by jobsearch-importers (https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers) for further processing, enrichment and import to OpenSearch.


## Create json file (legacy format)
Some people want un-enriched files in json format.
When you have created a file (and combined it with previous quarters if applicable), run:
 `python jsonlines_to_json.py <inputfile.jsonl> <outputfile.json>`

## Upload to data.jobtechdev.se
See instructions in OVERVIEW.md

### Optional: Splitting the year-file into files by publication month
After anonymization: 
set environment variable for monthly files output directory
run `python create_monthly_files.py`
N.B.: This has to be done an all published years regularly since ads in new historical files 


## Support

Create an issue in this project and use the label `Team Jobbdata`

## License

Apache 2, see the file LICENSE for details
